The user should launch the application and wait for the loading progress from 0 to 100% (fake loading). 
A prompt for permission to send notifications should appear. The user must allow or deny notification sending. 
The user should see a dynamically ranked rating of the top ten traders. The user can change the currency pair. 
Buttons Buy and Sell do nothing. Fields Timer and Investment only change their values internally. 
Screen Top 10 Traders: Every 5 seconds, random trader profits should fluctuate in the range from -150 to +50. 
The list is dynamic and should be sorted in descending order after each change. 
Screen Trade: Charts are displayed via a webview. If the user changes the pair on the Currency Pair screen, upon returning to the chart, it should refresh and display a different chart.


<img src="/uploads/324aec7e54b15046584cfad1ee852614/Simulator_Screen_Recording_-_iPhone_14_Pro_Max_-_2024-03-17_at_14.02.25.gif" width="320" >

<img src="/uploads/de03830bec3df38091e43466dfbd652f/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2024-03-17_at_14.02.29.png" width="320" >

<img src="/uploads/261fff3a818a3295ac27ee9e86d481fd/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2024-03-17_at_14.02.32.png" width="320" >

<img src="/uploads/6cb6e1f3db382741e8d906a924a6f5de/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2024-03-17_at_14.02.38.png" width="320" >

<img src="/uploads/3a47082bfe68afcdbf77f01975bdc135/Simulator_Screen_Shot_-_iPhone_14_Pro_Max_-_2024-03-17_at_14.02.46.png" width="320" >

