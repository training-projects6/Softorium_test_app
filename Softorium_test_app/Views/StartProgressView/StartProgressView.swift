//
//  ProgressView.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 14.03.2024.
//

import SwiftUI

struct StartProgressView: View {
    
    @State private var progressValue = 0.0
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                RoundedRectangle(cornerRadius: 20)
                    .foregroundColor(Color.gray)
                    .frame(height: 30)
                RoundedRectangle(cornerRadius: 20)
                    .foregroundColor(.myGreen)
                    .frame(width: geometry.size.width * CGFloat(progressValue / 100), height: 30)
                    .animation(.linear(duration: 2))
                HStack {
                    Spacer()
                    RollingNumberView()
                    Spacer()
                }
            }
        }
        .onAppear {
            withAnimation {
                progressValue = 100
            }
        }
    }
}
struct ProgressView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressView()
    }
}
