//
//  RollingNumberView.swift
//  rideupright
//
//  Created by Artem Soloviev on 17.01.2024.
//  Copyright © 2024 Sergei Kononov. All rights reserved.
//

import SwiftUI

struct RollingNumberView: View {
    
    var enteredNumber: Int = 100
    
    @State var total = 0
    
    var body: some View {
        Text("\(total)%")
            .font(.regular(size: 16))
            .foregroundColor(.white)
            .onAppear {
                addNumberWithRollingAnimation()
            }
    }
    
    func addNumberWithRollingAnimation() {
        if enteredNumber == 0 {
            self.total = 0
            return
        }
        
        withAnimation {
            
            let animationDuration = 2000
            let steps = min(abs(self.enteredNumber), 100)
            let stepDuration = (animationDuration / steps)
            
            total += self.enteredNumber % steps
            
            (0..<steps).forEach { step in
            
                let updateTimeInterval = DispatchTimeInterval.milliseconds(step * stepDuration)
                let deadline = DispatchTime.now() + updateTimeInterval
                
                DispatchQueue.main.asyncAfter(deadline: deadline) {
                    
                    var t = self.total
                    t += Int(self.enteredNumber / steps)
                    
                    if t > self.enteredNumber {
                        self.total = self.enteredNumber
                    } else {
                        self.total = t
                    }
                }
            }
        }
    }
}



