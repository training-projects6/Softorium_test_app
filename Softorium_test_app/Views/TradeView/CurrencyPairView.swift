//
//  CurrencyPairView.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 15.03.2024.
//

import SwiftUI

struct CurrencyButtonViev: View {
    
    @ObservedObject var vm: TradeViewModel
    var currency: CurrencyPair
    
    var body: some View {
        HStack {
            Button {
                vm.selectedPair = currency.pair
                vm.selectedID = currency.id
            } label: {
                Text(currency.pair)
                    .rectangleModifier(color: vm.selectedID == currency.id ?  .myGreen : .myGray, padding: 20)
            }
            .padding(5)
            .buttonStyle(.plain)
        }
    }
}

struct CurrencyPairView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var vm: TradeViewModel
    
    let columns = [GridItem(.adaptive(minimum: 150))]
    var body: some View {
        CustomZStack {
            VStack {
                LazyVGrid(columns: columns) {
                    ForEach(vm.currencyPair) { element in
                        HStack {
                            CurrencyButtonViev(vm: vm, currency: element)
                        }
                    }
                }
                Spacer()
            }.padding()
        } .toolbarModifier(clicked: { presentationMode.wrappedValue.dismiss() }, title: "Currency pair")
    }
}


