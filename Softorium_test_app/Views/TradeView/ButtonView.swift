//
//  SwiftUIView.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 15.03.2024.
//

import SwiftUI

struct ButtonView: View {
    
    @State private var investment = 0
    @State private var timeInSeconds = Date(timeIntervalSinceReferenceDate: 0)
    @State private var isEditingTimer = false
    @State private var isEditingInvestment = false
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "mm:ss"
        return formatter
    }()
    
    var body: some View {
        HStack {
            VStack {
                Text("Timer")
                    .font(.regular(size: 12))
                    .foregroundColor(.secondary)
                HStack {
                    
                    Button {
                        timeInSeconds -= 1
                        
                    } label: {
                        Image(systemName: "minus.circle")
                    }.buttonStyle(.plain)
                    
                    TextField("00:00", value: $timeInSeconds, formatter: dateFormatter , onEditingChanged: { editing in
                        self.isEditingTimer = editing
                        
                    })
                    .textFieldModifier()
                    .keyboardType(.numberPad)
                    
                    Spacer()
                    Button {
                        timeInSeconds += 1
                    } label: {
                        Image(systemName: "plus.circle")
                    }.buttonStyle(.plain)
                    
                }
            }.padding(.horizontal, 10)
                .rectangleModifier(color: .myGray, padding: 5)
                .padding(2)
                .overlay(isEditingTimer  ? RoundedRectangle(cornerRadius: 15).stroke(.myGreen, lineWidth:2) : nil)
            
            VStack {
                Text("Investment")
                    .font(.regular(size: 12))
                    .foregroundColor(.secondary)
                HStack {
                    Button {
                        if investment != 0 {
                            investment -= 1
                        }
                    } label: {
                        Image(systemName: "minus.circle")
                    }.buttonStyle(.plain)
                    Spacer()
                    
                    TextField("0", value: $investment, formatter: NumberFormatter(), onEditingChanged: { editing in
                        self.isEditingInvestment = editing
                    })
                    .textFieldModifier()
                    .keyboardType(.numberPad)
                    
                    Spacer()
                    
                    Button {
                        investment += 1
                    } label: {
                        Image(systemName: "plus.circle")
                    }.buttonStyle(.plain)
                    
                }
            }.padding(.horizontal, 10)
                .rectangleModifier(color: .myGray, padding: 5)
                .padding(2)
                .overlay(isEditingInvestment  ? RoundedRectangle(cornerRadius: 15).stroke(.myGreen, lineWidth:2) : nil)
        }
    }
}
