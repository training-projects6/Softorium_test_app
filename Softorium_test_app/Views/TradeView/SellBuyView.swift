//
//  SellBuyView.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 15.03.2024.
//

import SwiftUI

struct SellBuyView: View {
    var body: some View {
        HStack {
            Button {
                
            } label: {
                HStack {
                    Text("Sell")
                        .font(.regular(size: 24))
                    Spacer()
                }
                .rectangleModifier(color: .myRed,padding: 15)
                .padding(2)
            }.buttonStyle(.plain)
            Button {
                
            } label: {
                HStack {
                    Text("Buy")
                        .font(.regular(size: 24))
                    Spacer()
                }
                .rectangleModifier(color: .myGreen, padding: 15)
                .padding(2)
            }.buttonStyle(.plain)
        }
    }
}

struct SellBuyView_Previews: PreviewProvider {
    static var previews: some View {
        SellBuyView()
    }
}
