//
//  TopView.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 14.03.2024.
//

import SwiftUI

struct TradeView: View {
    
    @StateObject var vm = TradeViewModel()
    
    var body: some View {
        NavigationView {
            CustomZStack {
                VStack {
                    VStack {
                        Text("Balance")
                            .font(.regular(size: 12))
                            .foregroundColor(.secondary)
                            .padding(2)
                        Text("10000")
                            .font(.bold(size: 16))
                    }.rectangleModifier(color: Color("Gray"), padding: 5)
                        .padding(.horizontal, 20)
                    
                    if vm.selectedPair == "EUR / USD" {
                        WebViewModel(url: URL(string: vm.url1)!)
                    } else {
                        WebViewModel(url: URL(string: vm.url2)!)
                    }
                    
                    Group {
                        NavigationLink(destination: CurrencyPairView(vm: vm)) {
                            HStack {
                                Spacer()
                                Text(vm.selectedPair)
                                    .font(.bold(size: 16))
                                Spacer()
                                Image(systemName: "chevron.right")
                            }.rectangleModifier(color: .myGray, padding: 20)
                                .padding(2)
                        }.buttonStyle(.plain)
                        ButtonView()
                        SellBuyView()
                            .padding(.bottom, 10)
                    } .padding(.horizontal, 20)
                }
                .toolbarTitleModifier(title: "Trade")
            }
        }
    }
}

//struct TopView_Previews: PreviewProvider {
//    static var previews: some View {
//        TradeView()
//    }
//}
