//
//  TradeView.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 14.03.2024.
//

import SwiftUI

struct TopView: View {
    
    @StateObject var vm = TopViewModel()
    
    var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0
        return formatter
    }()
    
    var body: some View {
        NavigationView {
            CustomZStack {
                ScrollView(showsIndicators: false) {
                    VStack(alignment: .leading) {
                        HStack {
                          
                                Text("№")
                                    .frame(width: 40, height: 25)
                            Group {
                                Text("Country")
                                Text("Name")
                                Text("Deposit")
                                Text("Profit")
                                
                            } .frame(maxWidth: .infinity)
                        }
                        .padding()
                        .background(Rectangle()
                            .fill(.myGray)
                            .roundedCorner(10, corners: [.topLeft, .topRight]))
                        .font(.regular(size: 12))
                        .foregroundColor(.white)
                        
                        VStack(alignment: .leading) {
                            ForEach(Array(zip(vm.userArray.indices, vm.userArray)), id: \.1) { index, user in
                                
                                HStack(alignment: .center) {
                                   
                                        Text("\(index + 1)")
                                        .frame(width: 40, height: 30)
                                    Group {
                                        Image(user.flag)
                                        Text(user.name)
                                        Text(formatter.string(from: user.deposit as NSNumber)!)
                                        Text(formatter.string(from: user.profit as NSNumber)!)
                                            .foregroundColor(.myGreen)
                                    }.frame(maxWidth: .infinity)
                                }
                                .font(.regular(size: 14))
                                .foregroundColor(.white)
                                .padding(10)
                                .backgroundModifier(index: index)
                                
                            }
                        }
                    }.padding(5)
                }
            }
            .toolbarTitleModifier(title: "Top 10 Traders")
            .onAppear{
                vm.updater()
            }
        }
    }
}

struct TradeView_Previews: PreviewProvider {
    static var previews: some View {
        TopView(vm: TopViewModel())
    }
}
