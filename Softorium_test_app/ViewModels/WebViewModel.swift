//
//  WebView.swift
//  SearchImage
//
//  Created by Artem Soloviev on 06.02.2023.
//

import SwiftUI
import WebKit



struct WebViewModel: UIViewRepresentable {
    
    let url: URL
    func makeUIView(context: Context) -> some UIView {
        let webViewModel = WKWebView()
        let request = URLRequest(url: url)
        webViewModel.load(request)
        return webViewModel
    }
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}
