//
//  TradeViewModel.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 15.03.2024.
//

import SwiftUI

struct CurrencyPair: Hashable, Identifiable {

    var id = UUID()
    var pair: String
    
}

class TradeViewModel: ObservableObject {
    
    @Published var currencyPair: [CurrencyPair] = []
    @Published var selectedPair = "EUR / USD"
    @Published var selectedID = UUID()
    
    var url1 = "https://www.tradingview.com/chart/?symbol=BMFBOVESPA%3AEUR1%21"
    var url2 = "https://www.tradingview.com/chart/?symbol=NASDAQ%3AGBP"
    
    private let pair = ["EUR / USD", "GPB / USD"]
    
    init() {
        createPair()
    }
    
    func createPair() {
        for _ in 0...13 {
            if let currency = pair.randomElement() {
                currencyPair.append(CurrencyPair(pair: currency))
            }
        }
    }
}

