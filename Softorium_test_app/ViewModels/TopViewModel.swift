//
//  TradeViewViewModel.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 14.03.2024.
//


import SwiftUI
struct User: Identifiable, Hashable {
    let id = UUID()
    let name: String
    let flag: String
    let deposit: Int
    var profit: Int
    mutating func changeProfit() {
        profit += (Int.random(in: -50...150))
    }
}

class TopViewModel: ObservableObject {
    
    @Published var userArray:[User] = []
    
    let flags = ["USA", "France", "Germany", "India", "SKorea", "Spain", "Canada", "Brazil", "Australia", "Brazil"]
    let names = ["William", "James", "Oscar", "George", "Thomas", "Charley", "Jacob", "Harry", "Jack", "Oliver"]
    
    init() {
        makeModel()
    }
    
    func updater() {
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { timer in
            var indeces: Set<Int> = Set()
            
            let maxArrayCount = Int.random(in: 3..<self.userArray.count)
            
            for _ in 0...maxArrayCount {
                indeces.insert(Int.random(in: 0..<self.userArray.count))
            }
            for indece in indeces {
                self.userArray[indece].changeProfit()
                withAnimation(.easeInOut(duration: 0.5)) {
                    self.userArray = self.userArray.sorted{ $0.profit > $1.profit }
                }
            }
        }
    }
    
    func makeModel() {
        for _ in 0...9 {
            if let flag = flags.randomElement() {
                if let name = names.randomElement() {
                    let deposit = Int.random(in: 100...3000)
                    let profit = Int.random(in: 100...3000)
                    userArray.append(User(name: name, flag: flag, deposit: deposit, profit: profit))
                }
            }
        }
    }
}
