//
//  CustomZStack.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 14.03.2024.
//

import SwiftUI

struct CustomZStack<Content: View>: View {
    let content: Content
    let alignment: Alignment
    
    init(alignment: Alignment = .center, @ViewBuilder content: () -> Content) {
        self.content = content()
        self.alignment = alignment
    }
    
    var body: some View {
        ZStack(alignment: alignment){
            Color("Main")
                .ignoresSafeArea(.all)
            content
        }
    }
}

