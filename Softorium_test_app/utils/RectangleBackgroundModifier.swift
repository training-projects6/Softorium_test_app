//
//  RectangleBackground.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 15.03.2024.
//

import SwiftUI

extension View {
    
    func rectangleModifier(color: Color, padding: Double) -> some View {
       self
            .padding(padding)
            .frame(maxWidth: .infinity)
            .background(RoundedRectangle(cornerRadius: 15).fill(color))
            
    }
}
