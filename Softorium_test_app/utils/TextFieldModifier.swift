//
//  TextFieldModifier.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 19.03.2024.
//

import SwiftUI

extension View {
    func textFieldModifier() -> some View {
       self
            .multilineTextAlignment(.center)
            .keyboardType(.numberPad)
            .font(.bold(size: 16))
            
    }
}
