//
//  CustomColorsExtention.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 19.03.2024.
//

import SwiftUI

extension ShapeStyle where Self == Color {
    static var tabBar: Color {
        Color("TabBar")
    }
}

extension ShapeStyle where Self == Color {
    static var main: Color {
        Color("Main")
    }
}


extension ShapeStyle where Self == Color {
    static var myGray: Color {
        Color("Gray")
    }
}

extension ShapeStyle where Self == Color {
    static var myGreen: Color {
        Color("Green")
    }
}

extension ShapeStyle where Self == Color {
    static var myRed: Color {
        Color("Red")
    }
}
