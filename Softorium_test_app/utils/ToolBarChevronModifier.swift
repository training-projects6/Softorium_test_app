//
//  BackChevronView.swift
//  rideupright
//
//  Created by Artem Soloviev on 06.01.2024.
//  Copyright © 2024 Sergei Kononov. All rights reserved.
//

import SwiftUI


struct ToolbarModifier: ViewModifier {
    
    var clicked: () -> Void
    let title: String
    
    func body(content: Content) -> some View {
        return content
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button(action: clicked) {
                        Image(systemName: "chevron.backward")
                            .foregroundColor(.white)
                            .frame(width: 30, height: 30)
                            .font(Font.body.bold())
                    }
                }
                ToolbarItem(placement: .principal) {
                    Text(title)
                        .titleModifier()
                }
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitleDisplayMode(.inline)
    }
}

extension View {
    func toolbarModifier(clicked:  @escaping () -> Void, title: String) -> some View {
        modifier(ToolbarModifier(clicked: clicked, title: title))
    }
}



struct ToolbarTitleModifier: ViewModifier {
    
    
    let title: String
    
    func body(content: Content) -> some View {
        return content
            .toolbar {
            
                ToolbarItem(placement: .principal) {
                    Text(title)
                        .titleModifier()
                }
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitleDisplayMode(.inline)
    }
}
extension View {
    func toolbarTitleModifier(title: String) -> some View {
        modifier(ToolbarTitleModifier(title: title))
    }
}

