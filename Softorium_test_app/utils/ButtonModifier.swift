//
//  ButtonModifier.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 15.03.2024.
//

import SwiftUI

extension View {
    func buttonModifier() -> some View {
       self
            .padding()
            .frame(maxWidth: .infinity)
            .background(RoundedRectangle(cornerRadius: 10).fill(.myGray))
            .padding()
            
    }
}
