//
//  CustomFontExtension.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 19.03.2024.
//

import SwiftUI

extension Font {
    
    static func regular(size: CGFloat) ->Font {
        Font.custom("Inter-Regular", size: size)
    }
    
}

extension Font {
    
    static func bold(size: CGFloat) ->Font {
        Font.custom("Inter-Bold", size: size)
    }
    
}
