//
//  TitleModifier.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 15.03.2024.
//

import SwiftUI

extension View {
    func titleModifier() -> some View {
       self
            .font(.bold(size: 22))
            .foregroundColor(.white)
            .padding()
    }
}
