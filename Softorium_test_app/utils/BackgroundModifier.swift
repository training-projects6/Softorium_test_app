//
//  BackGround.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 14.03.2024.
//

import SwiftUI

struct BackGround: ViewModifier {
    
    let index: Int
    
    func body(content: Content) -> some View {
        
        if index == 9 {
            content
                .background(Rectangle()
                    .fill(Color("Gray"))
                    .roundedCorner(10, corners: [.bottomLeft, .bottomRight]))
        } else if !index.isMultiple(of: 2) {
            content
                .background(Rectangle().fill(.myGray))
        } else {
            content
        }
    }
}

extension View {
    func backgroundModifier(index: Int) -> some View {
        modifier(BackGround(index: index))
    }
}
