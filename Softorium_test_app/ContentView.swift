//
//  ContentView.swift
//  Softorium_test_app
//
//  Created by Artem Soloviev on 14.03.2024.
//

import SwiftUI
import UserNotifications

struct ContentView: View {
    
    @State private var selection = 0
    @State private var isShowingView = true
   
    
    init() {
        let tab = UITabBarAppearance()
        UITabBar.appearance().backgroundColor = UIColor(Color("TabBar"))
        UITabBar.appearance().standardAppearance = tab
        
        let tabapp = UITabBarItemAppearance()
        
        tabapp.selected.iconColor = UIColor(Color("Green"))
        tabapp.normal.iconColor = .gray
        tabapp.selected.titleTextAttributes = [.foregroundColor: UIColor(Color("Green"))]
        tabapp.normal.titleTextAttributes = [.foregroundColor: UIColor.gray]
        tab.stackedLayoutAppearance = tabapp
        
    }
    
    var body: some View {
        
        if isShowingView {
            VStack {
                StartProgressView()
                    .frame(height: 30)
            }
            .frame(maxHeight: .infinity)
            .background(Color.black)
            .ignoresSafeArea()
            .onAppear {
                
                Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { timer in
                    
                    isShowingView = false
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge]) { granted, error in
                        if granted {
                            print("Notification permissions granted")
                        } else {
                            print("Notification permissions denied")
                        }
                    }
                }
            }
        } else {
            
            TabView(selection: $selection) {
                
                TradeView().tabItem {
                    Image("Trade").renderingMode(.template)
                    Text("Trade")
                }
                .tag(0)
                
                TopView().tabItem {
                    Image("Top").renderingMode(.template)
                    Text("Top")
                }
                .tag(1)
            }
        }
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
